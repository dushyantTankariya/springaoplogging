package com.snippet.aspect;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LoggingAspect {

	@Before("execution(* com.snippet.customer.bo.CustomerBo.addCustomer(..))")
	public void logBefore(JoinPoint joinPoint) {
		System.out.println("AOP : logBefore() | " + joinPoint.getSignature().getName());
	}

	@After("execution(* com.snippet.customer.bo.CustomerBo.addCustomer(..))")
	public void logAfter(JoinPoint joinPoint) {
		System.out.println("AOP : logAfter() | " + joinPoint.getSignature().getName());
	}
	
	@AfterReturning(
			pointcut = "execution(* com.snippet.customer.bo.CustomerBo.addCustomerReturnValue(..))",
			returning= "result")
	public void logAfterReturning(JoinPoint joinPoint, Object result) {
		System.out.println("AOP : logAfterReturning() | " + joinPoint.getSignature().getName() + " | Method returned value is :" + result);
	}
	
	@AfterThrowing(
			pointcut = "execution(* com.snippet.customer.bo.CustomerBo.addCustomerThrowException(..))",
			throwing= "error")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
		System.out.println("AOP | logAfterThrowing() | " + joinPoint.getSignature().getName() + " | Exception : " + error);
	}
	
	
	@Around("execution(* com.snippet.customer.bo.CustomerBo.addCustomerAround(..))")
	public void logAround(ProceedingJoinPoint joinPoint) throws Throwable {
		System.out.println("AOP-1 | logAround() | " + joinPoint.getSignature().getName() + " | arguments : " + Arrays.toString(joinPoint.getArgs()));
		System.out.println("AOP-2 | Around before");
		joinPoint.proceed();
		System.out.println("AOP-3 | Around after");
	}
	
}