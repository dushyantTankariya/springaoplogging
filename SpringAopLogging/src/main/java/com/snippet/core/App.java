package com.snippet.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snippet.customer.bo.CustomerBo;

/**
 * @see <pre>
 * Common AspectJ annotations,
 * 1. Before        : Run before the method execution.
 * 2. After         : Run after the method returned a result.
 * 3. AfterReturning: Run after the method returned a result, intercept the returned result as well.
 * 4. AfterThrowing : Run after the method throws an exception.
 * 5. Around        : Run around the method execution, combine all three advices above.
 * </pre>
 * */
public class App {
	public static void main(String[] args) throws Exception {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("Spring-Customer.xml");

		CustomerBo customer = (CustomerBo) appContext.getBean("customerBo");
//		customer.addCustomer();
		
//		customer.addCustomerReturnValue();
		
//		customer.addCustomerThrowException();
		
		customer.addCustomerAround("Dushyant");

	}
}